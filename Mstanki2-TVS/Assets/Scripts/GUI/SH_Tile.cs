﻿using UnityEngine;
using System.Collections;

public class SH_Tile : MonoBehaviour {


	public New_GUI Mother;
	public float dead_value;
	bool alive ;
	public Texture2D Life;
	public Texture2D Death;

	void Start(){

		//transform.position = Mother.transform.position + new Vector3 (364-((dead_value-10)*1.2F),34F,0);
		alive = true;
	}


	void OnGUI(){

		if (alive) {
			GUI.DrawTexture(new Rect(0, 250+10*dead_value, 100, 100), Life);
		} else {
			GUI.DrawTexture(new Rect(0, 250+10*dead_value, 100, 100), Death);
		}


		
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerController.shield >= dead_value) {
			alive = true;
		} else {
			alive = false;
		}

		

		//
	}
}
