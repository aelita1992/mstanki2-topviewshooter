﻿using UnityEngine;
using System.Collections;

public class Bars : MonoBehaviour {

	public PlayerController player;


	private float currentLife=0;
	private float currentShield=0;
	private float currentEnergy = 0;

	private float currentAmmo = 0;
	private float currentAmmoMax = 0;

	private float currentRocket = 0;
	private float currentRocketMax = 0;

	private float currentGrenade = 0;
	private float currentGrenadeMax = 0;

	private float ammo_Ratio=0;
	private float grenade_Ratio=0;
	private float rocket_Ratio=0;

	private string ammo_msg ="";
	private string grenade_msg ="";
	private string rocket_msg ="";


	private GUIStyle HPStyle = null;
	private GUIStyle SHStyle = null;
	private GUIStyle AMStyle = null;
	private GUIStyle RCStyle = null;
	private GUIStyle GRStyle = null;
	private GUIStyle ENStyle = null;




	// Modyfikuje rozmiary uniwersalne pasków
	private float width_val= 0.0022f * Screen.width;
	private float heigth_val=0.06f * Screen.height;

	// Kanał alfa pasków (przezroczystość)
	private float alpha = 1.0f;

	private void InitStyles()
	{
		if( HPStyle == null )
		{
			HPStyle = new GUIStyle( GUI.skin.box );
			HPStyle.normal.background = MakeTex( 2, 2, new Color( 1f, 0f, 0f, alpha ) );

		}

		if( SHStyle == null )
		{
			SHStyle = new GUIStyle( GUI.skin.box );
			SHStyle.normal.background = MakeTex( 2, 2, new Color( 0f, 0f, 1f, alpha ) );

		}

		if( AMStyle == null )
		{
			AMStyle = new GUIStyle( GUI.skin.box );
			AMStyle.normal.background = MakeTex( 2, 2, new Color( 1f, 1f, 0f, alpha ) );

		}

		if( RCStyle == null )
		{
			RCStyle = new GUIStyle( GUI.skin.box );
			RCStyle.normal.background = MakeTex( 2, 2, new Color( 1f, 0.25f, 0f, alpha ) );

		}

		if( GRStyle == null )
		{
			GRStyle = new GUIStyle( GUI.skin.box );
			GRStyle.normal.background = MakeTex( 2, 2, new Color( 0f, 1f, 0f, alpha ) );

		}

		if( ENStyle == null )
		{
			ENStyle = new GUIStyle( GUI.skin.box );
			ENStyle.normal.background = MakeTex( 2, 2, new Color( 0f, 1f, 1f, alpha ) );
			
		}



	}
	
	private Texture2D MakeTex( int width, int height, Color col )
	{
		Color[] pix = new Color[width * height];
		for( int i = 0; i < pix.Length; ++i )
		{
			pix[ i ] = col;
		}
		Texture2D result = new Texture2D( width, height );
		result.SetPixels( pix );
		result.Apply();
		return result;
	}
	

	void OnGUI () {

		InitStyles ();

		//For example you have 100 life’s maximum.
		currentLife = PlayerController.HP;
		currentShield = PlayerController.shield;
		currentEnergy = PlayerController.energy;

		currentAmmo = Weapon.currentBullets;
		currentAmmoMax = Weapon.maxBullets;

		currentGrenade = WeaponManager.Get().getGrenadeAmmo();
		currentGrenadeMax = WeaponManager.Get().getGrenadeAmmoMax();

		currentRocket = WeaponManager.Get().getRocketAmmo();
		currentRocketMax = WeaponManager.Get().getRocketAmmoMax();


		ammo_Ratio = (currentAmmo / currentAmmoMax) * 100.0f;
		grenade_Ratio = (currentGrenade / currentGrenadeMax) * 100.0f;
		rocket_Ratio = (currentRocket / currentRocketMax) * 100.0f;

		ammo_msg = "Ammo: "+ currentAmmo.ToString("#") + " / " + currentAmmoMax.ToString("#");
		grenade_msg = "Grenades: "+ currentGrenade.ToString("#") + " / " + currentGrenadeMax.ToString("#");
		rocket_msg = "Rockets: "+ currentRocket.ToString("#") + " / " + currentRocketMax.ToString("#");

		if (currentLife > 0.0f) {
					//	GUI.Box (new Rect (10, 10, width_val * currentLife, heigth_val), "HP", HPStyle);
				}
		// Had to set it to five if dunno why - ask
		if (currentShield > 5.0f) {
					//	GUI.Box (new Rect (10, 20 + heigth_val, width_val * currentShield, heigth_val), "SHIELD", SHStyle);
				}

		if (currentAmmo > 0.0f) {
			GUI.Box (new Rect (400, 130 + 2.0f * heigth_val, width_val * 100.0f, heigth_val), ammo_msg, AMStyle);
				} else {
			GUI.Box (new Rect (400, 130 + 2.0f * heigth_val, width_val * 100.0f, heigth_val), "YOU ARE EMPTY", HPStyle);
				}

		if (currentGrenade > 0.0f) {
			GUI.Box (new Rect (400, 140 + 3.0f * heigth_val, width_val * 100.0f, heigth_val), grenade_msg, GRStyle);
		} else {
			GUI.Box (new Rect (400, 140 + 3.0f * heigth_val, width_val * 100.0f, heigth_val), "YOU ARE EMPTY", HPStyle);
		}

		if (currentRocket > 0.0f) {
			GUI.Box (new Rect (400, 150 + 4.0f * heigth_val, width_val * 100.0f, heigth_val), rocket_msg, RCStyle);
		} else {
			GUI.Box (new Rect (400, 150 + 4.0f * heigth_val, width_val * 100.0f, heigth_val), "YOU ARE EMPTY", HPStyle);
		
		if (currentEnergy > 5.0f) {
			//GUI.Box (new Rect (10, 60 + 5.0f * heigth_val, width_val * currentEnergy, heigth_val), "ENERGY", ENStyle);
		}
			//
	}
}

}