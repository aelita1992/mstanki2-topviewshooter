﻿using UnityEngine;
using System.Collections;

public class New_GUI : MonoBehaviour {

	int push_x=145;
	int push_y=8;

	public Texture2D Image;

	public Texture2D EMP;
	public Texture2D OneShoot;
	public Texture2D AsslRifl;
	public Texture2D Sniper;
	public Texture2D Shotgunn;
	public Texture2D Rocket;


	public Texture2D HPA;
	public Texture2D HPD;
	public Texture2D SHA;
	public Texture2D SHD;

	public Texture2D Bullet;
	public Texture2D Rockett;
	public Texture2D Grenade;


	Texture2D wpn ;

	public PlayerController player;
	bool alive = true;


	int currentAmmo;
	int currentAmmoMax;
	int currentGrenade;
	int currentGrenadeMax;
	int currentRocket;
	int currentRocketMax;
	string ammo_msg;
	string grenade_msg;
	string rocket_msg;


	//
	void Start(){

		transform.localScale = new Vector3(1.4F, 1.0F, 1.0F);

		//

		}

	void OnGUI(){

		if(WeaponManager.Get().getCurrentWeapon().name=="OneShooter")wpn = OneShoot;

		if(WeaponManager.Get().getCurrentWeapon().name=="Sniper Rifle")wpn = Sniper;
		
		if(WeaponManager.Get().getCurrentWeapon().name=="Rocket Launcher")wpn = Rocket;
		
		if(WeaponManager.Get().getCurrentWeapon().name=="Assault Rifle")wpn = AsslRifl;
		
		if(WeaponManager.Get().getCurrentWeapon().name=="Grenade Launcher")wpn = EMP;
		
		if(WeaponManager.Get().getCurrentWeapon().name=="Shotgun")wpn = Shotgunn;


		GUI.DrawTexture(new Rect(340+push_x, 250+push_y, 137, 90), wpn);
		GUI.DrawTexture(new Rect(190+push_x, 190+push_y, 525, 230), Image,ScaleMode.ScaleToFit);

		//markers

		for (int i =0; i<10; i++) {
			if(PlayerController.HP>i*10)GUI.DrawTexture(new Rect(486-i*5.5f+push_x, 272+push_y, 22, 22), HPA,ScaleMode.ScaleToFit);

				}

		for (int i =0; i<10; i++) {
			if(PlayerController.shield>i*10)GUI.DrawTexture(new Rect(480-i*5.5f+push_x, 290+push_y, 22, 22), SHA,ScaleMode.ScaleToFit);
			
		}

		for (int i =0; i<10; i++) {
			if(PlayerController.energy>i*10)GUI.DrawTexture(new Rect(477-i*5.5f+push_x, 309+push_y, 22, 22), SHA,ScaleMode.ScaleToFit);
			
		}


		for (int k =0; k<currentAmmo; k++) {
			GUI.DrawTexture(new Rect(0+k*2.0f, 0, 5, 10), Bullet,ScaleMode.ScaleToFit);
			
		}

		for (int k =0; k<currentGrenade; k++) {
			GUI.DrawTexture(new Rect(0+k*20.0f, 20, 20, 20), Grenade,ScaleMode.ScaleToFit);
			
		}

		for (int k =0; k<currentRocket; k++) {
			GUI.DrawTexture(new Rect(0+k*20.0f, 45, 10, 20), Rockett,ScaleMode.ScaleToFit);
			
		}


	//	GUI.DrawTexture(new Rect(0, 0, 5, 10), Bullet,ScaleMode.ScaleToFit);

		//
		


		//

	}


	// Update is called once per frame
	void Update () {
		//currentAmmo = WeaponManager.Get ().getRegularAmmo ();
		currentAmmo = (int)Weapon.currentBullets;
		//currentAmmoMax = Weapon.maxBullets;
		
		currentGrenade = WeaponManager.Get().getGrenadeAmmo();
		currentGrenadeMax = WeaponManager.Get().getGrenadeAmmoMax();
		
		currentRocket = WeaponManager.Get().getRocketAmmo();
		currentRocketMax = WeaponManager.Get().getRocketAmmoMax();
		

	}
}
