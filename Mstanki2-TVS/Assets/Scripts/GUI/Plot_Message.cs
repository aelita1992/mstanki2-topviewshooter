﻿using UnityEngine;
using System.Collections;

public class Plot_Message : MonoBehaviour {


	public string message;


	public Texture2D aTexture;

	void Start()
	{
		aTexture.Apply ();
	}
	
	
	public void Display_Message(string msg){

		GUI.DrawTexture(new Rect((Screen.width-160)/2, (Screen.height-20)/2, 160, 25), aTexture, ScaleMode.ScaleToFit, true);
		GUI.Box (new Rect ((Screen.width-160)/2, (Screen.height-20)/2, 160, 25),msg);

	}

	private bool cancelled = false;
	private bool active = false;


	void OnTriggerEnter2D(Collider2D other){

		//if (other.tag =! "Player") return;

		if (other.tag == "Player" && !cancelled) {
			//
			active = true;
			Debug.Log("Entered");
			
			
		}
		
	}


	void OnTriggerExit2D(Collider2D other){


		if (other.tag == "Player") {
			//
			active = false;
			cancelled=true;
			Debug.Log("Left");
			
			
		}
		
	}



	void Update(){

		if(active&&Input.GetKey (KeyCode.Escape))cancelled=true;

		}

	void OnGUI(){
	

	if(active&&!cancelled)Display_Message(message);


		}



}
