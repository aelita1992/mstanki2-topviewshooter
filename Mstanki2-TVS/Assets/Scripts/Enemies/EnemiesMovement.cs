﻿using UnityEngine;
using System.Collections;

public class EnemiesMovement : MonoBehaviour {

	public float movementSpeed;
	bool movingRight = true;
	float move = 1;
	private Vector3 dir;

	Animator animate;

	void Start () {
	
		animate = GetComponent<Animator> ();

		dir = new Vector2 (Random.Range (-100, 100), Random.Range (-100, 100));
		dir.Normalize ();
		Debug.Log (dir);
		//rigidbody2D.velocity = new Vector2 (move * movementSpeed, rigidbody2D.velocity.y); 


	}



	void Update () {
		animate.SetFloat ("speed", Mathf.Abs (move));

		transform.position = transform.position + dir * movementSpeed * Time.deltaTime;
	}

	void Flip(){
		
		movingRight = !movingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;

	}

	void OnCollisionEnter2D(Collision2D other)
	{
		//Debug.Log ("KOLIZJA");
		Debug.Log (dir);
		if (other.gameObject.tag != "Player") {
			/* if(dir.x >= 0 && dir.y >= 0)
			{
				dir = new Vector2 (-Random.value, -Random.value);
				//dir.Normalize ();
			}

			else if(dir.x >= 0 && dir.y <= 0)
			{
				dir = new Vector2 (-Random.value, Random.value);
				//dir.Normalize ();
			}

			else if(dir.x <= 0 && dir.y >= 0)
			{
				dir = new Vector2 (Random.value, -Random.value);
				//dir.Normalize ();
			}

			else if(dir.x <= 0 && dir.y <= 0)
			{
				dir = new Vector2 (Random.value, Random.value);
				//dir.Normalize ();
			}
			Debug.Log("! " + dir); */

			if(other.transform.position.x > this.transform.position.x)
			{
				dir = new Vector2(-Random.value, dir.y);
			}
			else if (other.transform.position.x < this.transform.position.x)
			{
				dir = new Vector2(Random.value, dir.y);
			}

			if(other.transform.position.y > this.transform.position.y)
			{
				dir = new Vector2(dir.x, -Random.value);
			}
			else if (other.transform.position.y < this.transform.position.y)
			{
				dir = new Vector2(dir.x, Random.value);
			}
		}
	}


}

