﻿using UnityEngine;
using System.Collections;

public class Enemy1 : Enemy {

	//public Animator animator;
	// Use this for initialization
	void Start () {
		lastPrimAttack=0;
		lastSecAttack=0;

		animator = GetComponent<Animator>();
		animator.Play("Idle");
	}

	public override void primAttack ()
	{
		if(lastPrimAttack + primAttackRate < Time.time)
		{
			PlayerController.hit(primAttackDMG);
			Debug.Log("HIT: " + primAttackDMG);
			lastPrimAttack = Time.time;
			animator.Play ("Attack");
		}
	}
}
