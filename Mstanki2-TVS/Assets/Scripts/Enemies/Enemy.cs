using UnityEngine;
using System.Collections;

public class Enemy: MonoBehaviour
{
    public GameObject player;
	public new string  name;
	public int HP;
	public float primAttackRate;
	public float secAttackRate;
	public int primAttackDMG;
	public int secAttackDMG;
	public int primAttackRange;
	public int secAttackRange;
	public float speed;
	public int visibilityRange;
	public bool isDead;
	protected float lastPrimAttack;
	protected float lastSecAttack;
	private bool isStuned = false;
	private bool isKnockbacked = false;
	private float stunTime = 0;
	private float knockbackTime = 0;
	private float sDuration;
	private float kDuration;
	private Vector3 knockbackVector;
	public Animator animator;
	public AnimationClip dieAnimation;
	public float turnSpeed = 5f;


	void Update()
	{
		if(isStuned == true && stunTime + sDuration < Time.time)
		{
			isStuned = false;
		}

		if(isKnockbacked == true && knockbackTime + kDuration < Time.time)
		{
			isKnockbacked = false;
		}

		transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f);
		//animator.Play ("Idle");
	}

	public virtual void primAttack()
	{
		Debug.Log("Attack1");
	}

	public virtual void secAttack()
	{
		Debug.Log ("Attack2");
	}

	public virtual void move(Vector3 dir, float speedFactor)
	{
		if (isDead == false) {
				float speed = this.speed;
				if (isStuned == true) 
					speed = speed / 10;
				if (isKnockbacked == true) {
					RaycastHit2D hit = Physics2D.Raycast (transform.position, knockbackVector, 8.5f);
					if (hit.collider != null) {
						if (hit.collider.gameObject.tag != "Wall")				
							this.transform.position += knockbackVector * speed / 1.3f * Time.deltaTime;
					} else {
						this.transform.position += knockbackVector * speed / 1.3f * Time.deltaTime;
					}
				}
				speed = speed * speedFactor;
				dir.Normalize ();
				this.transform.position += dir * speed * Time.deltaTime;

				float targetAngle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90;
				gameObject.transform.rotation = Quaternion.Slerp (gameObject.transform.rotation, Quaternion.Euler (0, 0, targetAngle), turnSpeed * Time.deltaTime);


				if (animator != null && speed != 0.0f)
			{
					animator.Play ("Walking");
				//if (GetComponentInChildren<Animator>()) GetComponentInChildren<Animator>().Play ("Walking");
			}
		}
	}

	public void hit(int i)
	{
		if(HP-i > 0) HP-=i;
		else die();
	}

	public void stun(float duration)
	{
		isStuned = true;
		stunTime = Time.time;
		this.sDuration = duration;
	}

	public void knockback(float duration, Vector3 knockbackVector)
	{
		isKnockbacked = true;
		knockbackTime = Time.time;
		this.knockbackVector = knockbackVector;
		this.kDuration = duration;
	}

	public virtual void die()
	{

		if (animator != null && isDead == false) {
			//animator.Play ("Idle");
			animator.Play ("Die");
			foreach (Transform childTransform in this.transform) {
				Destroy(childTransform.gameObject);
			}
			GameObject.Destroy (this.gameObject, dieAnimation.length-0.01f);
			isDead = true;
			Debug.Log ("debug");
		} //else
			//GameObject.Destroy (this.gameObject);

	}

}