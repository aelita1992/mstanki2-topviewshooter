﻿using UnityEngine;
using System.Collections;


public class Destroy_capsule : MonoBehaviour
{
	public int minus_points = 0;


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag != "Player" && other.tag != "Enemy")
			blow ();
		minus_points++;
	}
	public void blow()
	{
		GameObject.Destroy(this.gameObject);
		
	}
	
}