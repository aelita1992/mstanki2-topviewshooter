﻿using UnityEngine;
using System.Collections;

public class ChangeSprite : MonoBehaviour {

	public Sprite sprite;
	private SpriteRenderer renderer;

	void Start()
	{
		renderer = GetComponent<SpriteRenderer>();
	}

	public void change()
	{
		renderer.sprite = sprite;
	}
}
