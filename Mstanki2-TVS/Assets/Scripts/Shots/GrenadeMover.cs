﻿using UnityEngine;
using System.Collections;

public class GrenadeMover : MonoBehaviour {

	//public float speed;
	private bool isMoving;
	private Vector3 destination, dir, sp, desinationInWorld;
	private float speed;
	public ParticleSystem particle;
	private int dmg;
	private float stunDuration;
	// Use this for initialization
	void Start () {
		isMoving = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isMoving) {
			sp = Camera.main.WorldToScreenPoint(transform.position);
			destination = InputMapper.pointer;
			desinationInWorld = Camera.main.ScreenToWorldPoint(destination);
			dir = (destination-sp).normalized;
			speed = WeaponManager.Get ().getCurrentWeapon().getSpeed();
			dmg = WeaponManager.Get ().getCurrentWeapon ().getDMG();
			stunDuration = WeaponManager.Get ().getCurrentWeapon().stunDuration;

			isMoving = true;	
		}

		if (isMoving) 
		{
			transform.position = transform.position + dir * Time.deltaTime * speed;
			transform.position = new Vector3(transform.position.x, transform.position.y, 0);

			if(Vector2.Distance (desinationInWorld, transform.position) < 3.0f) blow();
			Debug.Log (Vector2.Distance (desinationInWorld, transform.position));

		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag != "Player" && other.tag != "Bullet" && other.tag != "Item" && other.tag != "Trigger")
		{
			blow ();
			//Debug.Log (other.tag);
		}
	}
	
	public void blow()
	{
		GrenadeBlow coll;
		particle.Play ();
		GameObject.Destroy(this.gameObject);
		Instantiate (particle, transform.position, transform.rotation);
		coll = GetComponentInChildren<GrenadeBlow>();
		for (int i=0; i<coll.enemies.Count; i++) 
		{
			coll.enemies[i].hit(dmg);
			coll.enemies[i].stun (stunDuration);
		}
		if (coll.player == true) PlayerController.hit (dmg);
	}

}
