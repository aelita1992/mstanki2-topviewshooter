﻿using UnityEngine;
using System.Collections;

public class EnemyBulletMover : MonoBehaviour {

	//public float speed;
	private bool isMoving;
	private Vector3 sp, dir;
	private float speed;
	private int dmg, s;
	private Vector3 playerPos;
	// Use this for initialization
	void Start () {
		isMoving = false;
		dmg = 15;
		speed = 500;
		s = 5;
		//player = GameObject.FindGameObjectWithTag("Player");
		playerPos = PlayerController.getPosition ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isMoving) {
			Vector3 spread = new Vector3(Random.Range (-s,s), Random.Range (-s,s));
			dir = (playerPos + spread - this.transform.position).normalized;
			
			isMoving = true;	
		}
		
		if (isMoving) 
		{
			transform.position = transform.position + dir * Time.deltaTime * speed;
			//transform.position = new Vector3 (transform.position.x, transform.position.y, 0);
		}
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			PlayerController.hit (dmg);
			GameObject.Destroy (this.gameObject);
			return;
		}
		if (other.tag == "Sciana_niszczalna")
		{
			Destroy_wall wall = other.GetComponent<Destroy_wall> ();
			wall.hit(dmg);
			GameObject.Destroy(this.gameObject);
			return;
		}
		if(other.tag != "Enemy" && other.tag != "Item" && other.tag != "Trigger")
			GameObject.Destroy(this.gameObject);
		
	}
}

