﻿using UnityEngine;
using System.Collections;

public class InputMapper : MonoBehaviour {
	public PlayerController player;
	private bool switchable;
	public static Vector3 pointer;
	private Vector3 mousePos;
	private bool mouseLook;
	private float horizontalTransform, verticalTransform;
	// Use this for initialization
	void Start () {
		switchable = true;
		mouseLook = false;
		horizontalTransform = verticalTransform = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if(!PlayerController.isDead)
		{
			//input klawiatury + przyciskow pada
			if(Input.GetButton ("P1 Left")) player.moveLeft();
			if(Input.GetButton ("P1 Right")) player.moveRight();
			if(Input.GetButton ("P1 Up")) player.moveUp();
			if(Input.GetButton ("P1 Down")) player.moveDown();

			if(Input.GetButton ("P1 Recharge")) PlayerController.shieldRecharge();

			if(Input.GetButton("P1 OneShooter")) WeaponManager.Get ().changeWeapon (1);
			if(Input.GetButton("P1 AssaultRifle")) WeaponManager.Get ().changeWeapon (2);
			if(Input.GetButton("P1 Shotgun")) WeaponManager.Get ().changeWeapon (3);
			if(Input.GetButton("P1 SniperRifle")) WeaponManager.Get ().changeWeapon (4);
			if(Input.GetButton("P1 RPG")) WeaponManager.Get ().changeWeapon (5);
			if(Input.GetButton("P1 EMP")) WeaponManager.Get ().changeWeapon (6);

			if(Input.GetButton ("P1 Shoot")) WeaponManager.Get ().getCurrentWeapon().shoot();

			//input axisów pada
			if(Input.GetAxis ("P1 Move Horizontal") < -0.2f) player.moveLeft ();
			if(Input.GetAxis ("P1 Move Horizontal") > 0.2f) player.moveRight ();
			if(Input.GetAxis ("P1 Move Vertical") > 0.2f) player.moveDown ();
			if(Input.GetAxis ("P1 Move Vertical") < -0.2f) player.moveUp ();

			if(Input.GetAxis ("P1 Switch Weapons") < -0.2f && switchable == true) 
			{
				WeaponManager.Get ().switchWeapon (-1);
				switchable = false;
			}
			if(Input.GetAxis ("P1 Switch Weapons") > 0.2f && switchable == true) 
			{
				WeaponManager.Get ().switchWeapon (1);
				switchable = false;
			}
			if(Input.GetAxis ("P1 Switch Weapons") == 0) switchable = true;

			if(Input.GetAxis ("P1 Shoot Axis") > 0.2f) WeaponManager.Get ().getCurrentWeapon().shoot();

			//obrot postaci

			if (mousePos == Input.mousePosition && (Mathf.Abs (Input.GetAxis ("P1 Look Horizontal")) > 0.2f ||
			                                        Mathf.Abs (Input.GetAxis ("P1 Look Vertical")) > 0.2f))
			{
				//Debug.Log ("mouse off");
				mouseLook = false;
			}
			
			if (mousePos != Input.mousePosition && (Mathf.Abs (Input.GetAxis ("P1 Look Horizontal")) < 0.2f ||
			                                        Mathf.Abs (Input.GetAxis ("P1 Look Vertical")) < 0.2f))
			{
				//Debug.Log ("mouse on");
				mouseLook = true;
			}

			if(mouseLook == false)
			{
					horizontalTransform = (Input.GetAxis ("P1 Look Horizontal") + 1) * Screen.width / 2;
					verticalTransform = (Input.GetAxis ("P1 Look Vertical") + 1) * Screen.height / 2;
					pointer = new Vector3 (horizontalTransform, verticalTransform, 0);
			} 
			else
			{
				//Debug.Log ("mouse on?");
				pointer = mousePos = Input.mousePosition;

			}






			/* stary input manager, nowy jest lepszy :D
			if(Input.GetKey (KeyCode.W)) player.moveUp();
			if(Input.GetKey (KeyCode.S)) player.moveDown();
			if(Input.GetKey (KeyCode.A)) player.moveLeft();
			if(Input.GetKey (KeyCode.D)) player.moveRight();

			if(Input.GetKey (KeyCode.LeftShift)) PlayerController.shieldRecharge();

			if(Input.GetKey(KeyCode.Alpha1)) WeaponManager.Get ().changeWeapon (1);
			if(Input.GetKey(KeyCode.Alpha2)) WeaponManager.Get ().changeWeapon (2);
			//if(Input.GetKey(KeyCode.Alpha3)) WeaponManager.Get ().changeWeapon (3); // SHOTGUN
			if(Input.GetKey(KeyCode.Alpha3)) WeaponManager.Get ().changeWeapon (4);
			if(Input.GetKey(KeyCode.Alpha4)) WeaponManager.Get ().changeWeapon (5);
			if(Input.GetKey(KeyCode.Alpha5)) WeaponManager.Get ().changeWeapon (6);
			
			if(Input.GetKey ("mouse 0")) WeaponManager.Get ().getCurrentWeapon().shoot();
			*/
		}
	}
}
