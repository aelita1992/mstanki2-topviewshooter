﻿using UnityEngine;
using System.Collections;

public class Weapon : ScriptableObject 
{
	public string name;

	protected int damage;
	protected int accuracy = 0;
	protected float fireRate;
	public int maxAmmo = 0;
	protected int magCapacity;	//pojemność magazynka
	public int currentAmmo = 0;	//posiadana amunicja
	protected int magAmmo;		//amunicja w magazynku
	protected float reloadTime;
	protected float speed;
	protected float nextFire = 0.0f;
	public float stunDuration;
	public static float maxBullets;
	public static float currentBullets;


	private bool isUnlocked;

	protected static GameObject shotBullet, shotRocket, shotGrenade, shotShotgun;
	protected static GameObject shotSpawn;

	public void setShot (GameObject b, GameObject r, GameObject g, GameObject s)
	{
		shotBullet = b;
		shotRocket = r;
		shotGrenade = g;
		shotShotgun = s;
	}

	public void setShotSpawn (GameObject s)
	{
		shotSpawn = s;
	}

	public float getSpeed()
	{
		return speed;
	}

	public int getAccuracy()
	{
		return accuracy;
	}

	public int getDMG()
	{
		return damage;
	}

	public virtual void shoot()
	{
		if (Time.time > nextFire) {
			Instantiate (shotBullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + WeaponManager.Get ().getCurrentWeapon ().fireRate;
		}
	}

	private void reload() 
	{

	}

	private void change()
	{

	}



}
