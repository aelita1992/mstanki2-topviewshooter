﻿using UnityEngine;
using System.Collections;

public class WeaponManager
{
	private static WeaponManager singleton;
	private Weapon[] weapons = new Weapon[6];

	private Weapon currentWeapon;
	private int currentWeaponNumber;
	private TextMesh mesh;
	
	private WeaponManager () 
	{
		weapons [0] = (Weapon) ScriptableObject.CreateInstance("OneShooter");
		weapons [1] = (Weapon) ScriptableObject.CreateInstance("AssaultRifle");
		weapons [2] = (Weapon) ScriptableObject.CreateInstance("Shotgun");
		weapons [3] = (Weapon) ScriptableObject.CreateInstance("SniperRifle");
		weapons [4] = (Weapon) ScriptableObject.CreateInstance("RocketLauncher");
		weapons [5] = (Weapon) ScriptableObject.CreateInstance("GrenadeLauncher");
	}

	public void setTextMesh(TextMesh m)
	{
		mesh = m;
	}

	public Weapon getCurrentWeapon() 
	{
		return currentWeapon;
	}

	public void changeWeapon(int n)
	{
		currentWeapon = weapons [n-1];
		currentWeaponNumber = n-1;
	//	TextMesh textObject = GameObject.Find("Test Text").GetComponent<TextMesh>();
		//Debug.Log ("changing");
		if(mesh != null)
			mesh.text = currentWeapon.name;
	}

	public void switchWeapon(int i)
	{
		int n = currentWeaponNumber+1 + i;
		changeWeapon(Mathf.Clamp(n, 1, 6));
		//Debug.Log ("switching");
	}

	public static WeaponManager Get()
	{
		if(singleton == null)
			singleton = new WeaponManager();
		
		return singleton;
	}

	public void pickAmmo(int q)
	{
		Weapon.currentBullets += q;
		if (Weapon.currentBullets > Weapon.maxBullets) Weapon.currentBullets = Weapon.maxBullets;
	}

	public void pickRocket(int q)
	{
		weapons[4].currentAmmo += q;
		if (weapons[4].currentAmmo > weapons[4].maxAmmo) weapons[4].currentAmmo = weapons[4].maxAmmo;
	}

	public void pickGrenade(int q)
	{
		weapons[5].currentAmmo += q;
		if (weapons[5].currentAmmo > weapons[5].maxAmmo) weapons[5].currentAmmo = weapons[5].maxAmmo;
	}

	public int getGrenadeAmmo()
	{
		return weapons[5].currentAmmo;
	}

	public int getGrenadeAmmoMax()
	{
		return weapons[5].maxAmmo;
	}

	public int getRocketAmmo()
	{
		return weapons[4].currentAmmo;
	}
	
	public int getRocketAmmoMax()
	{
		return weapons[4].maxAmmo;
	}

	public int getRegularAmmo()
	{
		return weapons[1].currentAmmo;
	}
	
	public int getRegularAmmoMax()
	{
		return weapons[1].maxAmmo;
	}


}