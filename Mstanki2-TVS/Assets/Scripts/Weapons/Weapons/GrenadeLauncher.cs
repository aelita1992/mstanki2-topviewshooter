﻿using UnityEngine;
using System.Collections;

public class GrenadeLauncher : Weapon
{
	public GrenadeLauncher() 
	{
		name = "Grenade Launcher";
		fireRate = 1.1f;
		speed = 350;
		currentAmmo = maxAmmo = 3;
		damage = 10;
		stunDuration = 3.0f;
	}

	public override void shoot()
	{
		if (Time.time > nextFire && currentAmmo > 0) {
			Instantiate (shotGrenade, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + fireRate;
			currentAmmo--;
		}
	}



}
