﻿using UnityEngine;
using System.Collections;

using Pathfinding;
//
public class AI : MonoBehaviour {

    protected bool isVisible;
    protected bool canAttack;
	protected Enemy enemy;
	//
	protected Seeker seeker;
	protected int currentWaypoint = 0;
	protected Path path;
	protected float nextWaypointDistance = 3;
	protected float pathTime=0;
	protected Vector3 dir;
	protected Vector3 lastPos;
	protected Vector3 originalPos;
	//protected Vector3 patrolPos;
	protected float rnd;
	//private Animator animator;

	protected enum State {IDLE, MOVE_DIR, MOVE_PATH, ATTACK};
	protected State state;

	protected float orgSpeed;
	private LayerMask layerMask;

	// Use this for initialization
	void Start () {
		state = State.IDLE;
		enemy = this.GetComponent<Enemy>();
		seeker = GetComponent<Seeker>();
		calcPath (enemy.transform.position);
		originalPos = enemy.transform.position;
		rnd = Random.Range (0.7f, 1.3f);
		orgSpeed = enemy.speed;
		//animator = enemy.animator;
	}
	
	// Update is called once per frame
	void Update () {
		if (isVisible)
		{

		}
	}



	protected bool checkVisibility()
	{
		Vector3 direction = PlayerController.getPosition() - transform.position;
		layerMask = 1 << 10 | 1 << 2;
		layerMask =  ~layerMask;
		RaycastHit2D hit = Physics2D.Raycast (GetComponentInParent<Transform>().position, direction, enemy.visibilityRange, layerMask);
		
		if(hit.collider != null && hit.collider.tag == "Player") 
		{
			//if (lastPos2==null) lastPos2=lastPos;
			lastPos = PlayerController.getPosition();
			return isVisible=true;
		} else return false;

	}
	
	protected bool checkAttack()
	{
		if(Vector3.Distance(this.transform.position, enemy.player.transform.position) <= enemy.primAttackRange) return canAttack=true;
		else return false;
	}

	protected void calcPath(Vector3 destination)
	{
		seeker.StartPath (this.transform.position, destination, OnPathComplete);
		currentWaypoint = 0;
	//	Debug.Log ("Path calculated");
	}

	protected void OnPathComplete (Path p) {
	//	Debug.Log ("Yay, we got a path back. Did it have an error? "+p.error);
		if (!p.error) {
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		}
	}
}
