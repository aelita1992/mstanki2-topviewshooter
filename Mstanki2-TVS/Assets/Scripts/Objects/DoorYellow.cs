﻿using UnityEngine;
using System.Collections;

public class DoorYellow : MonoBehaviour {
	
	public int openWidth = 100;
	public float transX = 1f;
	public float transY = 0f;
	public float openSpeed = 15f;
	
	bool isUnlocked = false;
	bool isOpen = false;
	bool isOpening = false;
	bool isClosing = false;
	private Vector3 open;
	int i = 0;
	
	PlayerController playerController;
	GameObject thePlayer;
	
	
	
	void Awake () {
		thePlayer = GameObject.FindGameObjectWithTag ("Player");
		playerController = thePlayer.GetComponent<PlayerController>();
		open = new Vector2 (transX, transY);
		open.Normalize ();
	}
	
	
	void Update () {
		if (isOpening) {
			transform.position = transform.position + open * openSpeed * Time.deltaTime;
			i++;
			if (i == openWidth) {
				isOpening = false;
				isOpen = true;
				i = 0; 
			}
			
		}
		if (isClosing) {
			transform.position = transform.position - open * openSpeed * Time.deltaTime;
			i++;
			if (i == openWidth) {
				isClosing = false;
				isOpen = false;
				i = 0;
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D other){
		if (playerController.card [0] == true) {
			Debug.Log ("Yellow door unlocked");	
			isUnlocked = true;
		}
		if (other.tag == "Player" && isUnlocked && !isOpen ) {
			Debug.Log ("Yellow door opened");
			isOpening = true;
		}
	}
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player" && isOpen) {
			Debug.Log ("Yellow door closed");
			isClosing = true;
		}
	}
	
}
