﻿using UnityEngine;
using System.Collections;

public class CardGreen : MonoBehaviour {
	
	PlayerController playerController;
	GameObject thePlayer;
	
	void Awake () {
		thePlayer = GameObject.FindGameObjectWithTag ("Player");
		playerController = thePlayer.GetComponent<PlayerController>();
	}
	
	
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			playerController.card [2] = true;
			Debug.Log ("Green Card = true");
			Destroy (this.gameObject);
		}
	}
}
