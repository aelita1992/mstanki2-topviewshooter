﻿using UnityEngine;
using System.Collections;

public class GUI_ammo : MonoBehaviour {

	public PlayerController player;
	// Use this for initialization
	void Start () {
		
	}





	// Update is called once per frame
	void Update () {
		if(WeaponManager.Get ().getCurrentWeapon ().name == "OneShooter")
		{
			this.guiText.text = WeaponManager.Get ().getCurrentWeapon().name + "  AMMO:inf/inf";
			return;
		}
		if(WeaponManager.Get ().getCurrentWeapon ().name == "GrenadeLauncher")
		{
			this.guiText.text = WeaponManager.Get ().getCurrentWeapon().name + "  AMMO: " + WeaponManager.Get ().getCurrentWeapon().currentAmmo + "/" + WeaponManager.Get ().getCurrentWeapon().maxAmmo;
			return;
		}
		if(WeaponManager.Get ().getCurrentWeapon ().name == "RocketLauncher")
		{
			this.guiText.text = WeaponManager.Get ().getCurrentWeapon().name + "  AMMO: " + WeaponManager.Get ().getCurrentWeapon().currentAmmo + "/" + WeaponManager.Get ().getCurrentWeapon().maxAmmo;
			return;
		}
		else 
			this.guiText.text = WeaponManager.Get ().getCurrentWeapon().name + "  AMMO: " + Weapon.currentBullets + "/" + Weapon.maxBullets;

	}
}
